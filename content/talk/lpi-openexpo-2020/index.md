---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Túneles y Agente de OpenSSH"
event: "LPI: OpenExpo Virtual Experience 2020"
event_url: https://www.lpi.org/es/articles/lpi-excited-support-openexpo-virtual-experience-2020-0
location:
address:
  street:
  city:
  region:
  postcode:
  country:
summary: Charla de @tonejito de LIDSoL en OpenExpo Europe 2020 - Sábado 20 de Junio de 2020 12:00 hrs (CST)
abstract: |-
    Veremos como exponer y alcanzar servicios internos y externos a través de las opciones LocalForward y RemoteForward de SSH, también analizaremos el proxy SOCKS que provee SSH con DynamicForward y cómo compartir el agente de SSH del equipo de escritorio con varios equipos de manera anidada para evitar copiar las llaves SSH.

    Estas herramientas son útiles para diversos fines como ingresar a recursos internos cuando se opera en el campo, acceder recursos bloqueados utilizando un equipo remoto como intermediario, exfiltrar datos después de una intrusión y en general para la administración de sistemas.

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2020-06-20T12:00:59-05:00
date_end: 2020-06-20T12:45:59-05:00
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: 2020-06-08T19:26:00-05:00

authors: ["tonejito"]
tags: ["lpi", "seguridad", "openssh", "lpic3", "linux"]

# Is this a featured talk? (true/false)
featured: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
links:
- name: Tonejito
  url: https://twitter.com/tonejito
  icon_pack: fab
  icon: twitter
- name: Tonejito
  url: https://tinyurl.com/Redes-Ciencias-UNAM-YouTube
  icon_pack: fab
  icon: youtube
- name: Agenda de LPI
  url: https://www.lpi.org/es/articles/lpi-excited-support-openexpo-virtual-experience-2020-0
- name: Agenda de OpenExpo
  url: https://openexpoeurope.com/es/virtual-experience/#agenda
- name: LPIconnect
  url: https://twitter.com/lpiconnect
  icon_pack: fab
  icon: twitter
- name: OpenExpoEurope
  url: https://twitter.com/OpenExpoEurope
  icon_pack: fab
  icon: twitter
- name: Conversión de huso horario
  url: https://www.timeanddate.com/worldclock/converter.html?iso=20200620T170000&p1=141&p2=155

# Optional filename of your slides within your talk's folder or a URL.
url_slides:

url_code:
url_pdf:
url_video:

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---
